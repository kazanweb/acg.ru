$(document).ready(function () {

	window.globalPopup = new Popup();

	// $(".js-combox").combox({
	// 	startFn: function(li, index, combox) {

	// 		this.input = combox.getElementsByTagName("input")[0];

	// 		this.input.value = li.getAttribute("value");

	// 	},
	// 	changeFn: function(li, index, combox) {

	// 		var _this = this;

	// 		this.input.value = li.getAttribute("value");

	// 	}
	// });

	$('#js-scroll-up').click(function() {
		$('html, body').animate({scrollTop: 0}, 500);
	});

	(function () {

		var jsMenuIn = document.querySelector('#js-menu-in');
		var jsMenuOut = document.querySelector('#js-menu-out');
		var footer = document.querySelector('#js-footer');
		var header = document.querySelector('#js-header');
		var trigger;
		var resize;

		if (!jsMenuIn || !jsMenuOut) {
			return false;
		}

		var slideout = new Slideout({
			'panel': document.getElementById('js-wrapper'),
			'menu': document.getElementById('js-menu-out'),
			'side': 'right',
			'padding': 280,
		});

		slideout.on('beforeopen', function () {
			footer.style.transition = 'transform 300ms ease';
			footer.style.transform = 'translateX(-280px)';
			header.style.transition = 'transform 300ms ease';
			header.style.transform = 'translateX(-280px)';
		});

		slideout.on('beforeclose', function () {
			footer.style.transition = 'transform 300ms ease';
			footer.style.transform = 'translateX(0px)';
			header.style.transition = 'transform 300ms ease';
			header.style.transform = 'translateX(0px)';
		});

		document.querySelector('.js-slideout-toggle').addEventListener('click', function () {
			slideout.toggle();
		});

		resize = function () {

			if (document.body.offsetWidth < 1200) {

				if (jsMenuIn.innerHTML != '') {
					jsMenuOut.appendChild(document.getElementById('js-menu-header'));
				}

			} else {

				if (jsMenuOut.innerHTML != '') {
					jsMenuIn.appendChild(document.getElementById('js-menu-header'));
				}

			}

		}

		resize();
		window.addEventListener('resize', resize);

	})();

	// disable portfolio
	// (function() {

	// 	var items = document.querySelectorAll('.js-menu-header__item');

	// 	$(items).each(function() {
	// 		var link = this.querySelector('a');
	// 		if(link.innerHTML.indexOf('Портфолио') === 0) {
	// 			link.removeAttribute('href');
	// 		}
	// 	});

	// })();

	var tooltipSlider = new Tooltip('tooltip');   // например, 'tooltip' - название класса тултипа

	$('.js-slider-clients__item').each(function() {

		$(this).click(function(e) {
			e.stopPropagation();
			tooltipSlider.init(this, {                 // инициализация, element - тег, по координатам которого выведем тултип
				offset: 10,                          // смещение относительно элемента
				close: true,                        // кнопка закрытия по умолчанию выводится
				position: 'top'                     // по умолчанию выводится сверху
			});
		    tooltipSlider.html(this.getAttribute('data-text'));                   // текст, который надо вывести
		    tooltipSlider.show();
			return false;                       // принудительно показываем на странице тултип

		});

	});

	document.body.addEventListener('click', function() {
		tooltipSlider.close();
	})

	$("[type=tel]").mask("+7 (999) 999-99-99");

	// Прибивка адаптивного футера к низу
	(function (footerSelector, wrapperSelector) {

		var footer = document.querySelector(footerSelector);
		var wrapper = document.querySelector(wrapperSelector);
		var height;
		var setSize;

		if (!wrapper || !footer) {
			return false;
		}

		setSize = function () {

			height = footer.offsetHeight;

			wrapper.style.paddingBottom = height + 'px';
			footer.style.marginTop = (height * (-1)) + 'px';

		}

		setSize();

		window.addEventListener('resize', setSize, false);

	})('#js-footer', '#js-wrapper');

});
