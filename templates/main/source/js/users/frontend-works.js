$(document).ready(function () {

	$(document).on('click', '[data-ajax]', function (e) {
		e.stopPropagation();
		e.preventDefault();
		$.get(this.getAttribute('data-url'), function (response) {
			globalPopup.html(response).show();
		});
	});

	$('#js-contact__formblock').submit(function() {
		$.get('ajax/response.html', function (response) {
			globalPopup.options({
				closeButtons: '.js-response__button',
				closeShow: false
			}).html(response).show();
		});
		return false;
	});

});
